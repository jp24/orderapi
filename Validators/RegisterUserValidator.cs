﻿using FluentValidation;
using OrderAPI.Entities;
using OrderAPI.Models;
using System.Linq;

namespace OrderAPI.Validators
{
    public class RegisterUserValidator : AbstractValidator<RegisterUserDto>
    {
        public RegisterUserValidator(OrderContext orderContext)
        {
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Password).MinimumLength(6);
            RuleFor(x => x.Password).Equal(x => x.ConfirmPassword);
            RuleFor(x => x.Email).Custom((value, context) =>
            {
                var userAlreadyExists = orderContext.Users.Any(user => user.Email == value);
                if (userAlreadyExists)
                    context.AddFailure("Email", "This email already exists.");
            }
            );
        }
    }
}
