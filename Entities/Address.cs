﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string TelNumber { get; set; }
        public string Email { get; set; }
        public virtual Customer Customer { get; set; }
        public int CustomerId { get; set; }
    }
}
