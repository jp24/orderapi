﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual List<OrderMerch> OrderMerch { get; set; }
        public int CustomerId { get; set; }
        public string MerchName { get; set; }
        public decimal MerchPrice { get; set; }
    }
}
