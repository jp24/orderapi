﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Entities
{
    public class OrderContext : DbContext
    {
        private readonly string _connectionString = "Server=(localdb)\\mssqllocaldb;Database=OrderDb; Trusted_Connection=True;";

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderMerch> OrderMerch { get; set; }
        public DbSet<Merch> Merch { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(u => u.Role);

            modelBuilder.Entity<Customer>()
             .HasOne(c => c.Address)
             .WithOne(a => a.Customer)
             .HasForeignKey<Address>(a => a.CustomerId);

            modelBuilder.Entity<Customer>()
             .HasMany(c => c.Orders)
             .WithOne(o => o.Customer);
            //.HasForeignKey(o => o.CustomerId);

            modelBuilder.Entity<OrderMerch>()
             .HasOne(om => om.Order)
             .WithMany(o => o.OrderMerch)
             .HasForeignKey(om => om.OrderId);

            modelBuilder.Entity<OrderMerch>()
             .HasOne(om => om.Merch)
             .WithMany(m => m.OrderMerch)
             .HasForeignKey(om => om.MerchId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}