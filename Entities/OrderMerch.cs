﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Entities
{
    public class OrderMerch
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public int MerchId { get; set; }
        public virtual Merch Merch { get; set; }
    }
}
