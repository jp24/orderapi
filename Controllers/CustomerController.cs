﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrderAPI.Entities;
using OrderAPI.Models;

namespace OrderAPI.Controllers
{
    [Route("api/customer")]
    [Authorize(Roles = "Admin, Moderator")]
    public class CustomerController : ControllerBase
    {
        private readonly OrderContext _orderContext;
        private readonly IMapper _mapper;

        public CustomerController(OrderContext orderContext, IMapper mapper)
        {
            _orderContext = orderContext;
            _mapper = mapper;
        }

        [HttpGet("{phoneNumber}")]
        public async Task<ActionResult<CustomerDetailsDto>> GetCustomer(string phoneNumber)
        {
            var customer = await _orderContext.Customers
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Address.TelNumber == phoneNumber);

            if (customer == null)
                return NotFound();

            var customerDetailsDto = _mapper.Map<CustomerDetailsDto>(customer);

            return Ok(customerDetailsDto);
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomer([FromBody] CustomerDetailsDto customerDetailsDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = _mapper.Map<Customer>(customerDetailsDto);

            if (customerDetailsDto == null)
                return NotFound();

            _orderContext.Customers.Add(customer);
            await _orderContext.SaveChangesAsync();

            int id = customer.Id;

            return Created("api/customer/" + id, null);
        }

        [HttpPut("{customerId}")]
        public async Task<IActionResult> ChangeCustomer(int customerId, [FromBody] CustomerDetailsDto customerDetailsDto)
        {
            var customer = await _orderContext.Customers
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Id == customerId);

            if (customer == null)
                return NotFound();

            customer.FirstName = customerDetailsDto.FirstName;
            customer.LastName = customerDetailsDto.LastName;
            customer.BirthDate = customerDetailsDto.BirthDate;
            customer.Address.City = customerDetailsDto.City;
            customer.Address.Street = customerDetailsDto.Street;
            customer.Address.PostCode = customerDetailsDto.PostCode;
            customer.Address.TelNumber = customerDetailsDto.TelNumber;
            customer.Address.Email = customerDetailsDto.Email;

            await _orderContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{phoneNumber}")]
        public async Task<IActionResult> RemoveCustomer(string phoneNumber)
        {
            var customer = await _orderContext.Customers
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Address.TelNumber == phoneNumber);

            if (customer == null)
                return NotFound();

            _orderContext.Customers.Remove(customer);
            await _orderContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
