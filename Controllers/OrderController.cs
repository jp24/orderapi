﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrderAPI.Entities;
using OrderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Controllers
{
    [Route("api/order")]
    [Authorize]
    public class OrderController : ControllerBase
    {
        private readonly OrderContext _orderContext;
        private readonly IMapper _mapper;

        public OrderController(OrderContext orderContext, IMapper mapper)
        {
            _orderContext = orderContext;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<List<OrderDto>> GetOrders()
        {
            var orders = _orderContext.Orders.ToList();
            var orderDtos = _mapper.Map<List<OrderDto>>(orders);

            return Ok(orderDtos);
        }

        [HttpGet("{customerId}")]
        public ActionResult<List<OrderDto>> GetOrderByCustomerId(int customerId)
        {
            var orders = _orderContext.Orders.Where(o => o.CustomerId == customerId).ToList();//FirstOrDefault(o => o.CustomerId == customerId);
            if (orders == null)
                return NotFound();

            var orderDtos = _mapper.Map<List<OrderDto>>(orders);

            return Ok(orderDtos);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder([FromBody] OrderDto orderDto)
        {
            var order = _mapper.Map<Order>(orderDto);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _orderContext.Orders.Add(order);
            await _orderContext.SaveChangesAsync();

            return Created("api/order/" + order.Id, null);
        }

        [HttpPut("{orderId}")]
        public async Task<IActionResult> ChangeOrder(int orderId, [FromBody] OrderDto orderDto)
        {
            var order = _orderContext.Orders.FirstOrDefault(o => o.Id == orderId);

            if (order == null)
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            order.MerchName = orderDto.MerchName;
            order.MerchPrice = orderDto.MerchPrice;

            await _orderContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{orderId}")]
        public async Task<IActionResult> RemoveOrder(int orderId)
        {
            var order = _orderContext.Orders.FirstOrDefault(o => o.Id == orderId);

            if (order == null)
                return NotFound();

            _orderContext.Orders.Remove(order);
            await _orderContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
