﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OrderAPI.Entities;
using OrderAPI.Models;

namespace OrderAPI.Controllers
{
    [Route("api/merch")]
    public class MerchController : ControllerBase
    {
        private readonly OrderContext _orderContext;
        private readonly IMapper _mapper;

        public MerchController(OrderContext orderContext, IMapper mapper)
        {
            _orderContext = orderContext;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<List<MerchDto>> GetAllMerch()
        {
            List<Merch> merchList = _orderContext.Merch.ToList();

            if (merchList == null)
                return NotFound();

            List<MerchDto> merchDtoList = _mapper.Map<List<MerchDto>>(merchList);

            return Ok(merchDtoList);
        }

        [HttpGet("{name}")]
        public ActionResult<MerchDto> GetMerchByName(string name)
        {
            var merch = _orderContext.Merch
                .FirstOrDefault(m => m.Name.Replace(" ", "-").ToLower() == name.ToLower());

            if (merch == null)
                return NotFound();

            var merchDto = _mapper.Map<MerchDto>(merch);

            return Ok(merchDto);
        }

        [HttpPost]
        public async Task<IActionResult> AddMerch([FromBody] MerchDto merchDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var merch = _mapper.Map<Merch>(merchDto);

            _orderContext.Merch.Add(merch);
            await _orderContext.SaveChangesAsync();

            int id = merch.Id;

            return Created("api/merch/" + id, null);
        }

        [HttpPut("{name}")]
        public async Task<IActionResult> ChangeMerch(string name, [FromBody] MerchDto merchDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var merch = _orderContext.Merch
                .FirstOrDefault(m => m.Name.Replace(" ", "-").ToLower() == name.ToLower());

            if (merch == null)
                return NotFound();

            merch.Name = merchDto.Name;
            merch.Price = merchDto.Price;

            await _orderContext.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{name}")]
        public async Task<IActionResult> RemoveMerch(string name)
        {
            var merch = _orderContext.Merch
                .FirstOrDefault(m => m.Name.Replace(" ", "-").ToLower() == name.ToLower());

            if (merch == null)
                return NotFound();

            _orderContext.Merch.Remove(merch);
            await _orderContext.SaveChangesAsync();

            return NoContent();
        }
    }
}
