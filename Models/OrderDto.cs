﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Models
{
    public class OrderDto
    {
        public int OrderId { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        [MinLength(1)]
        public string MerchName { get; set; }

        [Required]
        public decimal MerchPrice { get; set; }
    }
}
