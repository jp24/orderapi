﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Models
{
    public class MerchDto
    {
        [Required]
        [MinLength(1)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }
    }
}
