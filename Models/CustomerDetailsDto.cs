﻿using OrderAPI.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Models
{
    public class CustomerDetailsDto
    {
        [Required]
        [MinLength(2)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        public string LastName { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        [MinLength(2)]
        public string City { get; set; }

        [Required]
        [MinLength(2)]
        public string Street { get; set; }

        [Required]
        [MinLength(2)]
        public string PostCode { get; set; }

        [Required]
        [MinLength(2)]
        public string TelNumber { get; set; }
        public string Email { get; set; }
    }
}
