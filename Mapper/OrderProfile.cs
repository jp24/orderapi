﻿using AutoMapper;
using OrderAPI.Entities;
using OrderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Mapper
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Order, OrderDto>()
                .ForMember(oDto => oDto.OrderId, map => map.MapFrom(o => o.Id))
                .ReverseMap();

            CreateMap<Customer, CustomerDetailsDto>()
                .ForMember(cdDto => cdDto.City, map => map.MapFrom(customer => customer.Address.City))
                .ForMember(cdDto => cdDto.Street, map => map.MapFrom(customer => customer.Address.Street))
                .ForMember(cdDto => cdDto.PostCode, map => map.MapFrom(customer => customer.Address.PostCode))
                .ForMember(cdDto => cdDto.TelNumber, map => map.MapFrom(customer => customer.Address.TelNumber))
                .ForMember(cdDto => cdDto.Email, map => map.MapFrom(customer => customer.Address.Email))
                .ReverseMap();

            CreateMap<Merch, MerchDto>()
                .ReverseMap();
        }
    }
}
