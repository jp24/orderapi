﻿using OrderAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.DBTools
{
    public class DBSeeder
    {
        private readonly OrderContext _orderContext;

        public DBSeeder(OrderContext orderContext)
        {
            _orderContext = orderContext;
        }

        public void Seed()
        {
            if (_orderContext.Database.CanConnect())
                if (!_orderContext.Customers.Any())
                    InsertSampleData();
        }

        private void InsertSampleData()
        {
            List<Order> orders = MakeOrders();
            _orderContext.AddRange(orders);
            _orderContext.SaveChanges();
        }

        private List<Order> MakeOrders()
        {
            Merch merch = new Merch();
            merch.Name = "sok jabłkowy";
            merch.Price = 5.99m;

            return new List<Order>
            {
                new Order
                {
                    Customer = new Customer
                    {
                        FirstName = "Michał",
                        LastName = "Kowalski",
                        BirthDate = DateTime.Parse("02/12/1981"),
                        Address = new Address
                        {
                            City = "Wrocław",
                            Street = "Wrocławska",
                            PostCode = "12-345",
                            TelNumber = "123456789",
                            Email = "mk@abc.com"
                        }                  
                    },
                    OrderMerch = new List<OrderMerch>
                    {
                        new OrderMerch
                        {
                            Merch = merch
                        }
                    },
                    MerchName = merch.Name,
                    MerchPrice = merch.Price
                }
            };
        }
    }
}
